# Algoritmusok és adatszerkezetek 2. - 2. zh - AVL-fák

## Feladatok

* 1/a) – 6p
    * A táblán látható AVL-fából kiindulva végezzük el az alábbi lépéseket (i–beszúrás, d–törlés)
    * Minden lépést jelöljünk
    * Jelöljük a szükséges forgatásokat és végezzük is azokat el. Frissítsük az algoritmus alapján az egyensúlyi értékeket
    * <d–56; i–93; i–57; i–55; d–44>

* 1/b) – 2p
    * Legyen adott m ∈ ℕ (nem általad választott)
    * Van-e olyan m hosszú, ismétlődést nem tartalmazó kulcssorozat, aminek elemeit a megadott sorrendben egy kezdetben üres AVL-fába szúrva, úgy tudjuk az m méretű fát felépíteni, hogy a folyamat során egyszer se kelljen helyreállító forgatást végeznünk?
    * Milyennek kell m-nek lennie, hogy legyen ilyen sorozat?
    * Adjunk meg egy stratégiát ami tetszőleges megfelelő m-re generál egy ilyen tulajdonságú sorozatot
    * Mindent indokoljunk

* 2/a) – 6p
    * Írjunk algoritmust, ami kiírja a konzolra a hagyományos láncolt ábrázolással megadott AVL-fa összes olyan csúcsának címkéjét szigorúan növekvő sorrendben, ahol a csúcs jobb részfája magasabb, mint a bal részfája
    * Feltehetjük, hogy a fa helyes
    * Az algoritmus legyen a lehető leghatékonyabb

* 2/b) – 2p
    * A fenti algoritmusra igazak-e az alábbi állítások? A pont csak indoklással együtt jár
        * Bármilyen n-re lehet olyan n csúcsú helyes AVL-fát mondani, amire egy csúcs se kerül kiírásra
        * Bármilyen n-re, nem lehet olyan n csúcsú helyes AVL-fát mondani, amire n/2-nél több csúcs kerül kiírásra

## Megoldás

### 1/a)

* A táblán ez volt olvasható:
    
```
    56
   /  \
  44   82
 / \     \
32 48    91
    \
    55
```

* Törlésnél nem determinisztikus esetben a megszokottak szerint, a jobb részfa legkisebb elemét emeljük fel

* d - 56

```
    82--
   /  \
  44+  91
 / \
32 48
    \
     55
```

* (--,+) forgatás

```
    48=
   /  \
  44-  82=
 /     / \
32    55 91
```

* i - 93

```
    48+
   /  \
  44-  82+
 /     / \
32    55 91+
           \
            93
```

* i - 57

```
    48+
   /  \
  44-  82=
 /     / \
32    55+ 91+
       \   \
        57  93
```

* i - 55
* Nincs változás, mert nem lehet egy kulcs kétszer a fában

```
    48+
   /  \
  44-  82=
 /     / \
32    55+ 91+
       \   \
        57  93
```

* d - 44

```
    48++
   /  \
  32   82=
       / \
      55+ 91+
       \   \
        57  93
```

(++,=) forgatás

```
    82-
   /  \
  48+  91+
 /  \   \
32  55+  93
       \
        57
```

### 1/b)

* El lehet ezt pl. érni úgy, hogy a fát szintfolytonosan töltjük fel
    * Mivel szintfolytonos bejárás van, ezért ilyen sorozat bármilyen természetes szám m-re megadható
* Azaz az első elemhez képest a második kisebb, a harmadik nagyobb; majd a negyedik minden eddiginél kisebb, az ötödik az elsőnél kisebb, de a másodiknál nagyobb, stb.
* Ezt úgy lehet elérni, ha az m hosszú sorozat első elemének az m db elem növekvő sorba rendezése során meghatározható mediánját (vagy páros esetben a kettő medián közti kisebbet) választjuk meg; második elemnek a medián által kettéválasztott elemek közül a baloldali mediánját; harmadiknak a jobb oldali mediánját. Negyedik elem lehet a második elem által szétválasztott negyed közül a baloldali, és így tovább

### 2/a)

* Két dolgot kell tudnunk és használnunk:
    * Az AVL-fa keresőfa, azaz az inorder bejárása megegyezik a rendezés szerinti bejárással - ezt pedig egy egyszerű rekurzív algoritmussal meg tudjuk oldani Θ(n) műveletigénnyel
    * Az AVL-fa minden csúcspontja rendelkezik egy "b" adattaggal. Ennek, ha 1 az értéke, akkor az adott csúcs jobb részfája épp eggyel magasabb, mint a bal (többel pedig nem lehet magasabb)
* Innen az algoritmus:

```
printPlusNodes(t : Node*) : OutStream
    out := <>
    printPlusNodes(t, out)
    return out
```

```
printPlusNodes(t : Node*, out& : OutStream)
    HA t != NULL
        printPlusNodes(t->left, out)
        HA t->b = 1
            out.print(t->key)
        printPlusNodes(t->right, out)
```

* Alternatív megoldásként használhatjuk akár az általános fáknál megismert bejárási sémát, mivel a bináris láncolt ábrázolással technikailag azok is bináris fák:

```
printPlusNodes(t : Node*) : OutStream
    out := <>
    printPlusNodes(t, out)
    return out
```

```
printPlusNodes(t : Node*, out& : OutStream)
    AMÍG t != NULL
        printPlusNodes(t->left, out)
        HA t->b = 1
            out.print(t->key)
        t := t->right
```

* Nem rekurzív megoldás nem támogatott, mert még ha nagyságrendileg nem is, de abszolút értelemben biztosan több lenne a műveletigény, és az algoritmus komplexitása is nagyobb lenne
* Az akár elfogadható is lenne, ha nem a "b"-t használnánk, hanem on-the-fly számolnánk a magasságok különbségéből az egyensúlyokat
    * Ez is viszont csak akkor elfogadható, ha megmarad a műveletigény, tehát a magasságszámolás rekurzív függvényét valahogy össze tudjuk vonni a kiírás rekurzív függvényével
    * Ez pedig a jobb részfa esetében nem lehetséges, mivel később derülne ki a magasság, mint ahogy a szülőjét a magasság függvényében ki kéne írnunk
* Ezért a fenti megoldások az ideálisak

### 2/b)

* "Bármilyen n-re lehet olyan n csúcsú helyes AVL-fát mondani, amire egy csúcs se kerül kiírásra"
    * Ez igaz
    * Nézzünk néhány példát előbb:
        * n=0-ra nincs csúcs, nincs mit kiírni
        * n=1-re egy csúcs van, ami levél, következésképp 0 az egyensúlya
        * n=2-re a gyökér egyensúlya nem lehet 0, de ha a második csúcs kulcsa kisebb, mint az elsőé, akkor a bal részfájába kerül, következésképp a gyökér egyensúlya -1, tehát azt se írjuk ki
        * n=3-ra a teljes fa ilyen
        * n=4-re egy példa:
            ```
                3
               / \
              2   4
             /
            1
            ```

    * Általánosan: egy balra tömörített majdnem teljes fa. Ha ez keresőfa is egyben, akkor definíció szerint AVL-fa is lesz. Biztosan létezik is ilyen minden n természetes számra. Ekkor minden csúcs egyensúlya vagy "=", vagy az egyke csúcsok szülőinél (illetve őseinél) "-"-os, tehát egyet sem írunk ki
* "Bármilyen n-re, nem lehet olyan n csúcsú helyes AVL-fát mondani, amire n/2-nél több csúcs kerül kiírásra"
    * Az érzésünk az, hogy nagyságrendileg a csúcsok fele levél, amiknek "=" az egyensúlyuk, tehát nem írjuk ki őket
    * A kérdés, hogy lehet-e, hogy épp, hogy kevesebb, mint a fele levél, a többinek pedig mind "+"-os az egyensúlya, úgy, hogy jól tudjuk, "++"-os nem lehet már
    * Nyilván van olyan n (elég sok ilyen van), amire nem mondhatunk ilyen fát, de pl. n=7-re és n=12-re tudtunk konstruálni ilyet:

        ```
          3+
         /  \
        1+   5+
         \   /\
          2 4  6+
                \
                 7
        ```

        ```
            5+
           /  \
          2+   8+
         / \   /\
        1  3+  6+ 10+
            \  \ /  \
            4 7 9   11+
                      \
                       12
        ```

    * Az a kulcsmomentum, hogy az első példa 7-es csúcsa összesen 3 ősének balance-ára is "hat"; hasonlóan a második példa 12-ese 4 szülőre is
    * Tehát végülis az állítás hamis, mert van olyan n, amire lehet ilyen fát mondani
