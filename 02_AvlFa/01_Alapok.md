# AVL-fák

## Eredet és motiváció

* Georgij Makszimovics Adelszon-Velszkij (Георгий Максимович Адельсон-Вельский) és Jevgenyij Mihajlovics Landisz (Евгений Михайлович Ландис) szovjet matematikusok találmánya a 60-as évekből
* Olyan adatszerkezetre volt szükség, amivel kulcsos adatokra a CRUD műveletek (létrehozás, olvasás, módosítás, törlés) egyaránt és minden esetben elég gyorsak
* A kulcs a "kiegyensúlyozottság", amire az AVL-fa időrendben az első (de egyáltalán nem az egyetlen) példa (említhetnénk többek között a hangzatos nevű áldozatibárány-fát is (scapegoat tree))

## Definíció

* Egy olyan bináris keresőfa, aminek minden csúcsára igaz az AVL-tulajdonság, ami által magasság szerint kiegyensúlyozott fát kapunk
* Bontsuk ezt ki:
    * Bináris fa: olyan fa, ahol minden csúcsnak potenciálisan két gyereke és ezáltal két részfája van, a bal és jobb (és számít, hogy melyik melyik)
        * Ha valamelyik gyerek üres fa (azaz láncolt reprezentáció esetén NULL), akkor az egy üres részfa
        * Levélnek hívjuk azt a csúcsot, ahol mindkét részfa üres
        * Létezhet olyan implementáció is, ahol a csúcsok ismerik a saját szülő csúcsukat, az AVL-fa alap implementációjában ez nem teljesül
    * Bináris keresőfa: egy olyan bináris fa, ahol minden csúcsra igaz, hogy annak bal részfájában az adott csúcs címkéjénél (kulcsánál) csupa kisebb címkéjű, míg a jobb részfájában csupa nagyobb címkéjű elem van
        * Két dolgot érdemes tisztázni: egyenlőség nem lehet a fában, ha ezt megengedjük, akkor (csak) rendezfával van dolgunk (minden keresőfa rendezőfa is, de nem minden rendezőfa keresőfa)
        * A fenti állítás rekurzívan is igaz, azaz nem kizárólag a direkt gyerekekről, hanem a részfabeli összes elemről szól (nemcsak azért lett így megfogalmazva, hogy a bal és/vagy jobb gyerek nélküli csúcsokra ne kelljen külön kitérni - ugyanis az üres részfákra végülis ugyanúgy igaz az állítás), nézzünk egy példát, ami nem keresőfa:
    
    ```
        6
       / \
      4   8
         /
        5    
    ```
    
    
    * Az AVL-tulajdonság igaz az x csúcsra, ha |h(left(x)) - h(right(x))| < 2
        * Azaz ha az x csúcs bal és jobb részfájának a magasság-különbsége maximum 1
        * Az üres (rész)fa magassága definíció szerint -1, az egy csúcsúé 0, stb., a fenti példa fájának magassága 2, az AVL-tulajdonság igaz minden csúcsára, de mégse AVL-fa, mert nem keresőfa
        * Nyilván az 1-et el kell fogadnunk, mint lehetséges magasságkülönbség, mert enélkül nem lehetne a kiegyesúlyozott állapotok közötti átmeneteket megvalósítani - nem lehetne páratlan sok csúcsa egy ilyen fának
        * Egyébként ha minden csúcs gyerekeinek magasságkülönbsége 0, teljes fáról kell beszélnünk
    * A magasság szerinti kiegyensúlyozottság csak annyit jelent, hogy nem engedhetjük, hogy a részfák magasságai minden korlát nélkül széttartsanak, tehát ez az AVL-tulajdonságnál valamivel enyhébb fogalom, de a félév során ezt gyakorlatilag azonosnak vesszük, hiszen a magasság szerint kiegyensúlyozott fák közül az AVL-fákon kívül még a B+-fákat fogjuk említeni, amikre voltaképpen ugyanúgy teljesül ez a viszonylag szigorű feltétel
        * A magasság szerint kiegyensúlyozott fákra röviden kiegyensúlyozott fákként fogunk hivatkozni
        * Ismert még az önkiegyensúlyozó bináris fa kifejezés is. Ezt is azonos értelemben használjuk, mint a kiegyensúlyozottat, hiszen mint látni fogjuk a fát módosító algoritmusokat úgy határozzuk meg, hogy a kiegyensúlyozottságot sose ronthassák el - az egy invariáns tulajdonság legyen

## Műveletek

* A szokásos keresőfa-műveleteket használhatjuk, azaz
    * Kereshetünk
        * Ez ugyanúgy működik, mint sima keresőfánál, adott k kulcs keresése esetén kiindulunk a gyökérből, ha a k kisebb, mint a gyökér kulcsa, balra, ha nagyobb, akkor jobbra megyünk. Ezt ismételjük, amíg nem lesz egyenlő a gyökér kulcsa k-val, ekkor megtaláltuk a keresett elemet; vagy ha nem érünk egy üres részfához, ekkor nem tartalmaz k kulcsú elemet - a keresőfa-tulajdonság miatt k egyedi lesz
    * Beszúrhatunk
        * A hagyományos keresőfára vett beszúrás egy kereséssel indul: ha megtaláljuk az elemet, kilépünk, mondván nem szúrhatjuk be újra ugyanazt, ha elértük az egyik üres részfát, felvesszük a helyére
        * Mivel esetünkben ez megboríthatja a kiegyensúlyozottságot (szélsőséges esetben akár csúcsok száma - 1 is lehet a fa magassága), ilyenkor a beszúrás részeként az előzőekben megadott módszer után kiegyensúlyozó forgatásokat is végzünk
    * Törölhetünk
        * Itt is előbb megkeressük kulcs alapján az elemet. Ha nincs találat, a törlés sikertelen; ha pedig van, azt a csúcsot töröljük, helyére pedig a keresőfa-tulajdonságot nem elrontva annak egy leszármazott-csúcsát rakjuk
        * Ezt a műveletet most is kiegészítjük forgatásokkal a kiegyensúlyozottságot helyreállítandó
* A konkrét műveletekről részletesebben kicsit később
* Műveletigények:
    * Minden műveletet a keresés előz meg. A keresés legrosszabb esetben a fa magasságával korrelál, mivel tudjuk, hogy a fa kiegyensúlyozott, ez azt jelenti, hogy, ha n a csúcsok száma, ez pontosan log<sub>2</sub>(n), azaz ez egy logaritmikus műveletigényű algoritmus
    * A másik két műveletnél még egy csúcs be- vagy kiláncolásáról van szó, ami önmagában konstans, valamint a helyreállításokról, amik garantálják, hogy a famagasság megtartsa logaritmikus mértékét
    * Látni fogjuk, a "kiegyensúlyozások" vagy "forgatások" mindig néhány csúcs cseréjét jelentik úgy, hogy a hozzájuk tartozó részfák nem változnak, esetleg csak annyiban, hogy a szólő csúcsuk változik. Azt is látni fogjuk, hogy beszúrásnál maximum 1, törlésnél maximum famagasságnyi ilyen forgatás történhet - bár legrosszabb esetben a beszúrásnál is famagasságnyi csúcsot kell ahhoz vizsgálnunk, hogy kiderüljön melyik is a forgatásban érintett csúcshalmaz
    * Ezekből megállapíthatjuk, hogy láncolt ábrázolás esetén (ahol a csúcsok átláncolása valóban konstans, hiszen nem kell résztömböket shiftelni), a teljes műveletigény mindhárom esetben logaritmikus
