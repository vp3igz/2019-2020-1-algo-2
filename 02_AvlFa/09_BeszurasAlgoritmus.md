# AVL-fa - Beszúrás

* Ez a fejezet órán nem hangzott el, viszont a struktogramok egy az egyben megtalálhatók az előadásanyagban
* Az előző fejezet "d"változójának logikáját ezzel a jóval egyszerűbb példán keresztül könnyebben megérthetjük

```
insert(&t : Node*, k : T, &d : L)
    HA t = NULL
        t := new Node(k)
        d := true
    KÜLÖNBEN
        HA k < t-key
            insert(t->left, k, d)
            HA d
                leftSubtreeGrown(t,d)
        HA k > t->key
            insert(t->right, k, d)
            HA d
                rightSubtreeGrown(t,d)
        HA k = t->key
            d := false
```

* A tipikus rekurzív keresés
    * Ha t NULL, leértünk az aljára, oda "ahol lennie kéne", de nem volt ott -> azaz ide beszúrhatjuk
    * Ha nem NULL, hanem nagyobb, mint a beszúrandó, nézzük meg a jobb részfát
    * Ha fordítva, a balt
    * Ha megtaláltuk, SKIP
* Ezen kívül gondozzuk még a d változót, ami akkor lesz true, ha a rekurzió adott szintjén a részfa magassága nőtt
    * Ekkor a szülő részfát esetleg forgatni kell
    * Értelemszerűen az egész legalján, ha beszúrunk akkor nőtt az az eredetileg -1, most már 0 méretű egy csúcsú részfa, ahova szúrtunk; ha pedig már megtaláltuk az elemet, biztosan nem nő semmi
    * Egyébként pedig a két xSubtreeGrown függvény hivatott ezt a növekedést kezelni: frissíti a balance-okat, ha kell propagálja a növekedést, ha kell lekezeli egy forgatással

```
leftSubtreeGrown(&t : Node*, &d : L)
    HA t->b = -1
        l := t->left
        HA l->b = -1
            balanceMMm(t,l)
        KÜLÖNBEN
            balanceMMp(t,l)
        d := false
    KÜLÖNBEN
        t->b := t->b - 1
        d := t->b < 0
```

* Az alapszituáció, hogy beszúrtunk az előző algoritmus legelső ágán
* Ez, hacsak nem a gyökérből értünk rögtön ide (üres fa), azt jelenti, hogy egy csúcs bal vagy jobb gyerekét adtuk hozzá a rendszerhez
* Amikor a rekurzió visszatér a szülőhöz, az előző algoritmus 2. vagy 3. ágánál a "HA d" résznél leszünk ekkor úgy, hogy t a frissien beszúrt csúcs szülője
* Mondhatjuk ekkor, hogy pl. ha balra szúrtunk, bizony nőtt a bal részfa. Innen az a kérdés, milyen teendőnk van nekünk ezzel
* Ha eddig a balance -1 volt, akkor most ezzel, hogy a bal nőtt, -2 lett, azaz "--". Ekkor tehát vagy (--,-) vagy (--,+) forgatást kell csinálnunk a gyerek alapján (aki biztosan van, különben nem lehetne kettős a "-" súly)
* Ha a forgatást abszolváltuk (ezeket az előző fejezetben és az előadásjegyzetben találjátok), d lehet már hamis, mert tovább biztos nem kell forgatni
* Ha nincs "--"-os egyensúly, akkor "-"-os lehet, mert eddig "="-s volt, vagy "="-s lehet, mert eddig "+"-os volt. Más nem lehet, mert az AVL-tulajdonság invariáns
* Csökkentjuk eggyel a csúcs egyensúlyát
* Majd azt mondjuk, d akkor legyen igaz, ha most "-" lett. Azaz, ha eddig "=" volt. Miért is?
    * Ha eddig "="-s volt a szülő, akkor volt két egyforma magasságú részfája. Bármelyikbe is szúrtunk (bár tudjuk, hogy a balba), annak a magassága nőtt, következésképpen az egész fáé is, ami esetleg az ő szülőjénél produkálhat egy "--" vagy "++" balance-t. Ha eddig "+"-os volt a csúcs, akkor épp most egyenlítettük ki a bal részfa magasságát, tehát "="-s lett, beszúrásnál meg olyan forgatás nincs, amit az "="-s csúcsok triggerelnének, így hát d lehet hamis, megállhat a rekurzió
