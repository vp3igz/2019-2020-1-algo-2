# AVL-es vizsgapélda

* Így szólott a 2018. 01. 30-ai akkor még Algo 1-es vizsga hetedik felvonásának második feladata
* Összesen 20 pontot lehetett kiosztani, ennek eloszlását jelöltem

## Feladatszöveg

* 2.a, A bináris keresőfákkal kapcsolatos fogalmakat ismertnek feltételezve, mondja ki az AVL fa meghatározásához szükséges definíciókat!
* 2.b, Szemléltesse a 3 beszúrását és a 8 törlését, mindkét esetben az { [ 1 (2) ] 4 [ ({5} 6 {7} 8 (9) ) ] } AVL fára! (Törléskor, indeterminisztikus esetben a jobb részfa minimumát használjuk!) Jelölje, ha ki kell egyensúlyozni, a kiegyensúlyozás helyét, és a kiegyensúlyozás után is rajzolja újra fát! A rajzokon jelölje a belső csúcsoknak az algoritmus által nyilvántartott egyensúlyait is, a szokásos módon!
* 2.c, Rajzolja le a hat általános kiegyensúlyozási séma közül azokat, amiket alkalmazott

## Megoldás

* 2.a, - 3 pont
    * Az AVL-fa magasság szerint kiegyensúlyozott bináris keresőfa
    * Egy bináris keresőfa magasság szerint kiegyensúlyozott, ha minden t csúcsára igaz, hogy |h(right(t)) - h(left(t))| < 2
* 2.b, - Kiindulás felrajzolása: 3 pont, a két végigjátszás: 5-5 pont
    * Kiindulás:
    ```
       4+
     /    \
    1+     8-
     \    / \
      2  6=  9
        / \
       5   7
    ```
    * 3 beszúrása:
    ```
        4+
      /    \
     /      \
    1++      8-
     \      / \
      2+   6=  9
       \  / \
        3 5  7
    ```
    * (++,+) forgatás:
    ```
        4+
      /    \
     /      \
    2=       8-
   / \      / \
  1   3    6=  9
          / \
          5  7
    ```
    * Figyeljünk oda, hogy a gyökérnek meghagytuk a "+"-os egyensúlyt, habár már nem lenne az, de a rekurzió nem fog feljutni oda, hogy frissítse
    * A levelekhez nem írtam egyensúlyt, azok értelemszerűen "="-sők
    * Újra a kiindulás:
    ```
       4+
     /    \
    1+     8-
     \    / \
      2  6=  9
        / \
       5   7
    ```
    * 8 törlése:
    ```
       4+
     /    \
    1+     9--
     \    /
      2  6=
        / \
       5   7
    ```
    * (--,=) forgatás:
    ```
       4+
     /    \
    1+     6+
     \    / \
      2  5   9-
            /
           7
    ```
* 2.c, - 2-2 pont
    * A (++,+) és a (--,=) sémák felrajzolása, lásd előző fejezetek