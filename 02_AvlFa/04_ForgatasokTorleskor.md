# AVL-fa - Törlés

* A most következőkben azt fogjuk vizsgálni, hogyan lehet AVL-fából kulcs alapján elemet törölni
* Megkeressük az érintett elemet, kiláncoljuk, majd attól függően, pontosan honnan is töröltük, mindenféle kiegyensúlyozatlansági eseteket kellhet kezelnünk
* A beszúrásnál ismertetett 4 séma itt is jelen van, ezt egészítjük ki további 2-vel
* A latin betűk most is csúcsot, a görög betűk részfát hivatottak reprezentálni

## A (++,=) forgatás

* Törlés után egy adott csúcs egyik részfájának csökkenhet a mérete. Ez a méretcsökkenés a törölt csúcs szülője, vagy a gyökér felé vezető úton bármelyik csúcs esetében akár egy "++"-os vagy "--"-os egyensúlyig is fajulhat
* Az eddig tanult 4 eset mind előállhat. Ezen kívül a fa ilyen alakot is felvehet:

```
  x
 / \
α   y
   /\
  β  γ
```

* Ahol α magassága h, β és γ magassága pedig h+1
* Azaz y egyensúlya "=", de x-é "++", hiszen a jobb részfája h+2 magas
* A megoldás a következő forgatás:

```
   y
  / \
 x   γ
/ \
α  β
```

* Itt is sikerült a 2 különbséget eliminálni, de valamennyi egyensúlytalanság maradt a rendszerben
* x egyensúlya "+", y egyensúlya "-"
* Még egy fontos dolgot észrevehetünk: mivel β h+1 magas, y bal részfája h+2 magas, az egész építmény h+3 magas. Mekkora volt eredetileg? Eredetileg β magassága miatt x jobb részfája volt h+2 magas, az egész fa itt is h+3 magas volt!
    * Azaz ez a forgatás az érintett fa méretét nem csökkentette
    * Ez bizonyos szempontból nem szerencsés: minél kompaktabb a fa, annál hatékonyabbak a műveletek, tehát ha egy forgatás nem minden ágat tesz rövidebbé (nem csökken a fa mérete), az nem annyira ideális forgatás
    * Viszont képzeljük el, hogy ez az eredetileg x-szel, végül y-nal fémjelzett fa egy nagyobb fa része. Ha most ennek csökkent volna a magassága, akkor ha pl. a szülője "+"-os volt és ez a fa a bal részfája volt, akkor most "++"-ssá vált!
    * Azaz, törlésnél a fa méretét csökkentő (++,+), (++,-), (--,-), (--,+) forgatások bár alacsony szinten megoldanak egy kiegyensúlyozottságot, magasabb szinten okozói lehetnek. Törlésnél ezért nem elégszünk meg az érintett csúcstól a gyökér felé vezető úton talált első kiegyensúlyozottság javításával, utána ezen az úton tovább kell nézelődni (a többi részfa nem romolhat el)
    * Ez alól felmentést élvez a (++,=) és (--,=) forgatás, hiszen ott nem csökken a faméret, nem gyűrűzik tovább a csökkenés

## A (--,=) forgatás

* Ugyanaz:

```
    x
   / \
  y   γ
 /\
α  β  
```

* α és β magassága h+1, azaz y balance-a "="
* γ magassága h, azaz x balance-a "--"
* Forgatás után:

```
  y
 / \
α   x
   /\
  β  γ
```

* x egyensúlya "-"
* y egyensúlya "+"