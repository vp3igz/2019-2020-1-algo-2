# AVL-fa - Törlés

## Törlés keresőfából - ismétlés

* Álljon itt ismétlésül néhány példa arra, hogy is törlünk keresőfából. Az AVL-fa egy keresőfa. Amikor AVL-ből törlünk, előbb mint keresőfából kell szabályosan kitörölni az érintett csúcsot, majd ezután lehet az AVL-tulajdonság helyreállításával foglalkozni
* Az előző fejezet példája egy levelet törölt, ez egy egyszerű eset. Nézzük most át az összes lehetséges esetet:
* Mindig x-szel fogom a törlendő csúcsot jelölni

### Ha a törlendő csúcs levél

```
  a
 / \
x   b
```

* Egyszerűen csak kiszedjük, a másik gyerek iránya nem változik. Ezután jöhet szükség esetén a szülő kiegyensúlyozása:

```
  a
   \
    b
```

### Ha a törlendő csúcs egy gyerekes

```
  a
 / \
x   b
 \
  c
```

* Mindegy, a csúcs melyik oldali gyereke is volt, ha a csúcs bal gyereke volt a-nak, akkor a c mindenképp kisebb a-nál, ha netán jobb gyereke, mindenképp nagyobb, tehát úgymond helyettesíthető a szülő csúcs pozíciójával
* Egyszerűen felhúzzuk a gyereket (és az egész részfáját, ha van):

```
  a
 / \
c   b
```

### Ha a törlendő csúcs két gyerekes

```
    a
   / \
  x   b
 / \
c   d
   / \
  e   f
```

* A keresőfa-tulajdonság fenntartása a célunk, azaz a törlésre ítélt csúcsot az inorder (keresőfánál: rendezés szerinti) bejárás szerinti megelőzőjével vagy rákövetkezőjével kell helyettesíteni
* Ez vagy a bal részfa legnagyobb, vagy a jobb részfa legkisebb eleme
* Nincs semmilyen egzakt szempont, hogy melyik legyen (lehetne az, amelyik a nagyobb, de ehhez számon kéne tartani az "alulról vett magasságot" is)
* Ha mást nem mondunk, egyezményesen a jobb részfa legkisebb elemével cserélünk, ami ebben a példában speciel szerencsésebb is
* Azért megadom mindkét esetet
* Bal részfa legnagyobb elemével való csere esetén:

```
    a
   / \
  c   b
   \
    d
   / \
  e   f
```

* Jobb részfa legkisebb elemével:

```
    a
   / \
  e   b
 / \
c   d
     \
      f
```
