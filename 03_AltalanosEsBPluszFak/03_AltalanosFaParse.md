# Általános fa

# Írásos forma

* Vezessünk be az általános fákra egy egyértelmű írásos megadási módot
* Minden csúcs zárójelben van, a zárójel után közvetlenül a címke, azután sorban (zárójelben) a gyerek csúcsok
* A zárójelek fajtája mindegy, csak a jobb olvashatóság miatt használtunk több félét (hierarchia köztük nincs)
* Az előző fejezetek fája pl. így ábrázolható: ```(a[b(c)(d{e(f)})(g[h][i])][j])```
* Az alábbi fa pedig így: ```{1[2(5)](3)[4(6)(7)]}```

```
  1
 /|\
2 3 4
|  / \
5  6 7
```

# Parse-olás

## Iteráltan

* Parse-oljuk fel a fenti formából a binárisan láncolt ábrázolású fát
* Karakterenként olvashatunk, nem lépkedhetünk vissza az olvasás során, és nem is tudjuk előre, milyen hosszú lesz az input (azaz InStreamet használhatunk)
* Feltehetjük, hogy az input helyes
* Feltehetjük, ha egy címke több jegyű, azt is egyszerre tudjuk beolvasni (azaz nem karakterenként, hanem tokenenként olvasunk)
* Következzen az iteratív, azaz ciklust tartalmazó megoldás:

```
parse(in : InStream) : Node*
	c := in.read()
	HA c eleme NYITO_ZJ
		c := in.read()
		t := new Node(c)
		s = Stack<Node*, {CHILD,SIBLING}>()
		s.push((t, CHILD))
		c := in.read()
		AMÍG c != EOF
			HA c eleme NYITO_ZJ
				c := in.read()
				n := new Node(c)
				HA s.top()₂ = CHILD
					s.top()₁ -> child1 := n
					s.top()₂ := SIBLING
				KÜLÖNBEN
					s.pop()₁ -> sibling := n
				s.push((n, CHILD))
			KÜLÖNBEN
				HA s.top()₂ = CHILD
					s.top()₂ := SIBLING
				KÜLÖNBEN
					s.pop()
			c := in.read()
		return t
	KÜLÖNBEN
		return NULL
```

* Az elején olvasunk egyet (mert különben semmit nem tudunk az inputról)
    * Ez vagy EOF lett - ekkor üres fát olvastunk fel - lásd a legvége...
    * ... vagy nyitó zárójel
        * Ekkor ha újabbat olvasunk, meglesz a gyökér címkéje - ez biztos, hogy létezik is, különben nem lett volna zárójel
        * Csinálunk egy vermet, amiben mindig azt az "utat" fogjuk számon tartani, aminek a végére éppen most olvassuk be a következő elemet
        * Két dolgot tartunk számon minden elemről:
            * Mi maga a csúcs, aminek a gyerekét vagy testvérét hozzá kell adnunk - ez egy Node pointer, és úgy vesszük, hogy már a t-vel fémjelzett fában van (vegyük észre, hogy t-vel returnölünk, de t-nek csak egyszer adunk értéket, itt "t" nem csak egy ciklusváltozó), azaz, ha erre az elemre kötünk rá egy új gyereket vagy testvért, azt egyúttal az épülő fába is kötjük
            * A másik dolog, hogy az adott elemnél egy gyereket (CHILD) vagy egy testvért (SIBLING) várunk-e. Ez egy két értékű enum, akár bool is lehetne
                * Ez nem egy evidens dolog. Kicsit olyan, mintha a nyitó-csukó zárójelpárokat számolnánk több szinten. Ha egy elemnek még NULL a gyereke, akkor még lehet, hogy lesz gyereke, ha már nem NULL, akkor már nem lesz (max. testvére annak a gyereknek). Ez eddig könnyű: de ha bezáródik az elem körüli zárójel, akkor már biztosak lehetünk benne, hogy ha amúgy követi majd őt egy másik csúcs, az biztos nem a gyereke lesz. Ez a dolog, tehát - hogy a csúcs pointereinek NULL-ellenőrzésével nem tudjuk eldönteni biztosan, hogy lehet-e még gyereke/testvére -, egy olyan környezetfüggő feltétel, amit ezzel a segédváltozóval tudunk megfogni
        * A verembe tehát berakjuk a gyökeret a "CHILD" értékkel, ami tehát azt jelenti, hogy "a gyökér volt az utolsó parse-olt elem, aminek a gyerekeit várjuk a következőkben"
        * Utána olvasunk újra, mivel előre olvasunk, és megindul a ciklus
        * Két dolog lehetett: nyitó vagy csukó zárójel. Bármi is volt, utána olvasunk újra (lásd next() a felsorolóknál; ill. variánsfüggvény progmódból)
            * Ha nyitó volt, akkor rögtön be is olvassuk a címkét, majd a (létező) veremtető segítségével eldöntjuk, hogy az utoljára beolvasott elemmel milyen viszonyban is van ez az új elem
                * Ha az CHILD-et várt, akkor beállítjuk annak a csúcsnak a gyerekének, és az ábrázolás sajátosságai miatt biztosra vehetjük, hogy ez a pointer már nem íródik felül, ennek a csúcsnak már csak SIBLING-je lehet. Az új csúcsot pedig berakom CHILD-dal a verembe, hiszen utána már lehet, hogy neki lesz gyereke...
                * Különben, ha az SIBLINGET várt, akkor ez az elem már biztosan nem fog változni, ki is veszem a veremből, de előtte siblingjévé teszem az újat, amit így rakok be a verembe. Azaz a veremben az új elem, mint a testvérek közötti eddigi legkisebb fog szerepelni, felette pedig annak ősei, akiknek még lehetnek testvérei!
            * Ha nem nyitó volt, akkor csukó
                * Ekkor ha a legutóbbi csúcs "gyerekei között" voltunk, a zárójel ezt zárja le, tehát ennek az elemnek nem lesz már gyereke, ámde testvére még lehet
                * Ha testvért vártunk, akkor pedig semmit nem várunk már, kivesszük a csúcsot dicstelen

## Rekurzívan

* Az ilyen jellegű feladatokat sokkal rövidebben, egyszerűbben lehet rekurzívan megoldani, viszont az algoritmus kitalálása kitekertebb gondolkodást igényelhet

```
parse(in : InStream) : Node*
	c := in.read()
	HA c eleme NYITO_ZJ
		return parseRec(in)
	KÜLÖNBEN
		return NULL
```

* Itt is előre olvasunk, érezzük, hogy a gyökér esete kicsit más, valahogy be kell indítani a rekurziót
* Rekurzív függvényeknél ez a fajta megoldás igen gyakori, hogy egy viszonylag kis komplexitású az egyszeri teendőket megadó függvénnyel körbevesszük a voltaképpeni rekurziót
    * A fenti iterált függvénynél is a gyökeret feldolgoztuk már a cikluson kívül - mellesleg a verem használata is egy intő jel arra, hogy rekurzív algoritmust kéne használni, hiszen a rekurzió is a hívási vermet használja (ki)
* A különben ág itt az EOF esete, tehát amikor üres a fa

```
parseRec(in : InStream) : Node*
	c := in.read()
	t = new Node(c)
	c := in.read()
	HA c eleme NYITO_ZJ
		t->child1 := parseRec(in)
	c := in.read()
	HA c eleme NYITO_ZJ
		t->sibling := parseRec(in)
	return t
```

* Az elején akkor megyünk be, ha van gyökér, azaz ha épp egy nyitó zárójel után vagyunk. Ezért bátran olvashatunk, amit olvasunk az biztosan címke lesz - olyan nincs, hogy a zárójelpár üres legyen
* Megyünk tovább, ha most megint nyitót olvastunk, akkor tulajdonképpen ugyanezt megismételhetjük úgy, hogy az épülő fa az eddigi csúcs gyereke lesz!
* Ha nem nyitó volt, akkor záró, ami azt jelenti, hogy gyereke nem lesz ennek a csúcsnak, de testvére még lehet
* Ha most újra nyitót olvastunk, akkor tehát beolvassuk a testvérbe rekurzívan az újabb részfát
* A végén vagy zárót, vagy (a legkülső szinten) EOF-ot olvastunk, a lényeg, hogy nem nyitót
