# Általános fák

## Levélszám - preorder alapon

* Állapítsuk meg, hány levél van a paraméterként megadott általános fában
* Tulajdonképpen egy bejárást kell felokosítani, hiszen minden csúcsról el kell tudni dönteni, levél-e. Használjuk (pl.) a preorder bejárást

```
countLeaves(t : Node*) : N
	c := 0
	countLeavesPreorder(t, c)
	return c
```

```	
countLeavesPreorder(t : Node*, c& : N)
	AMÍG t != NULL
		HA t->child1 = NULL
			c := c+1
		KÜLÖNBEN
			countLeavesPreorder(t->child1, c)
		t := t->sibling
```

* Láthatjuk, a preorder bejárásnál a "process" a c növelése a megfelelő feltétel mellett
* Ez a feltétel épp ellentétes azzal, amikor van további gyerek, így a továbblépést a hamis ágba rakhattuk
* Az elején még a rekurzióba belépés előtt el kellett indítani c-t

## Levélszám - rekurzív alapon

* Egy még egyszerűbb kis függvény
* Mindenképp továbblépünk a gyerek és a testvér felé is, de ha speciel a vizsgált csúcs levél (attól még testvére lehet!), akkor hozzáadunk egyet a gyűjtendő értékbe
* Most nem segédváltozóba gyűjtünk, hanem returnölünk

```
countLeaves(t : Node*) : N
	HA t = NULL
		return 0
	KÜLÖNBEN HA t->child1 = NULL
		return 1 + countLeaves(t->sibling)
	KÜLÖNBEN
		return countLeaves(t->child1) + countLeaves(t->sibling)
```
