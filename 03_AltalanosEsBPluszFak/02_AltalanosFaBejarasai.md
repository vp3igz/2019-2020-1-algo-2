# Általános fák

## Nevezetes bejárások

* Álljon itt mégegyszer az előző fejezet példája:

```
    a
   / \
  b   j
 /|\
c d g
 /  | \
e   h  i
|
f
```

* Értelmezzük itt is a preorder és a postorder bejárást
    * Előbbi az, amikor előbb az adott csúcsot, majd a gyerekeit a rendezés sorrendjében érintjük; utóbbi amikor előbb a gyerekeket "balról jobbra", majd a csúcsot
    * Inorder nincs, hiszen annak csak akkor lenne értelme, ha két gyerek lenne, akik között érinthetnénk a szülő csúcsot
* Valamint a szintfolytonos bejárás is a bináris fával analóg módon adott, tehát előbb a gyökér, majd a gyökér gyerekei balról jobbra, majd balról jobbra a gyerekek gyerekei külön-külön is balról jobbra, stb.
* A preorder bejárás a fenti fára: <a, b, c, d, e, f, g, h, i, j>
* A postorder bejárás: <c, f, e, d, h, i, g, b, j, a>
* A szintfolytonos bejárás: <a, b, j, c, d, g, e, h, i, f>

## Algoritmusok

* A preorder és postorder kódja rekurzív és iteratív egyszerre
* Rekurzív, mert fa; iteratív, mert nem fix számú gyerek van
* A preorder elég logikus, a postorder elsőre annyira nem, ott abba érdemes belegondolni, ha előbb váltanánk a következő testvérre, és utána írnánk ki, bizonyos csúcsokat egyáltalán ki se írnánk

```
preorder(t : Node*)
    AMÍG t != NULL
        process(t->key)
        preorder(t->child1)
        t := t->sibling
```

```
postorder(t : Node*)
    AMÍG t != NULL
        postorder(t->child1)
        process(t->key)
        t := t->sibling
```

* Azt érdemes megfigyelni, hogy itt a "t"-t egyfajta futó változóként használjuk, amit megtehetünk, mivel nem referencia szerint adtuk át

```
levelorder(t : Node*)
    q := Queue()
    q.add(t)
    AMÍG !q.isEmpty()
        t := q.first()
        process(t->key)
        t := t->child1
        AMÍG t != NULL
            q.add(t)
            t := t->sibling
```

* Ez a tavalyról jól ismert elv, csupán annyi változott, hogy nem fix számú gyereket kell hozzáadni, emiatt a belső ciklus