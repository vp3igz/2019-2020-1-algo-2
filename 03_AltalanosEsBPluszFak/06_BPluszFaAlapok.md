# B+-fa

* A B+-fa egy újabb adatszerkezet kulcsos adatok rendezett tárolásához, kereséséhez
* A gyakorlatban a relációs adatbázisok megvalósításásánál használják
* A cél az, hogy minden művelet (keresés, beszúrás, törlés) egyaránt logaritmikus legyen (a tárolt adatok számának függvényében), és lehetőleg minden lépés, amikor keressük a megfelelő elemet, egy egységes méretű blokkot olvasson, ami hatékonyabb a lemezes tárolókon
* A B+-fákhoz tartozik egy paraméter, ami meghatároz néhány alapvető korlátot, ezt a B+-fa fokának (degree, order) nevezzük és d-vel jelöljük
    * d > 3, természetes szám
* A példákban d mindig 4 lesz

## Felépítése

* Mint általában, itt is csomópontok és az azokból kiinduló élek vannak, amik egy-egy másik csomópontra mutató pointerek
* Itt nincs általános aritás (azaz, hogy a fa bináris-e), se teljes szabadság (általános fák), hanem a típusdefiníció szintjén minden csomópontnak rögzített kereteken belüli, 0 és d közötti gyereke lehet
    * Ez valójában még szigorúbb, amit látni fogunk, ha megértjük a fa működését
* Egy csomópont vizuálisan így néz ki:

```
 ___________
| 3 |   |   |
-------------
| *| *| /| /|
|__|__|__|__|
```

* A doboz alsó részében a csomópontra jellemző k db slot van (k kisebb-egyenlő, mint d - ill. levélszinten élesen kisebb, mint d)
* A slotokban pointerek vannak, a levélszint kivételével ugyanilyen típusú csomópontokra mutató pointerekkel
* Mint látható, az adatszerkezet "fizikailag" d slotot enged meg, de ebből csak az első k-t használjuk, a maradék NULL
* A levélszint kivételével egy k slotú csomóponthoz k-1 ún. hasítókulcs is tartozik, amiket a felső blokkban szemléltetünk
* Ezek a kulcsok afféle határszámok, útjelző táblák a pointerek számára. Úgy értelmezzük őket, hogy az első ilyen kulcs "választja ketté" az első két pointert, a második a második és harmadik pointert, a harmadik a harmadik és negyedik pointert, azaz a k-1. a k-1. és a k. pointert. Ezért van eggyel kevesebb kulcs, mint pointer
* Kicsit konkrétabban, ha a példabeli Node-ot nézzük az első pointer olyan részfa gyökerére mutat, ahol az összes kulcs -∞ és 3-1, azaz 2 közötti. A második pointer pedig a hasítókulcstól kezdődő kulcsokat tartalmazó Node-okat, azaz a 3-től ∞-ig levő kulcsokat tartalmazza
* Nézzünk egy bonyolultabb példát:

```
 ___________
| 3 | 8 |   |
-------------
| *| *| *| /|
|__|__|__|__|
```

* Itt két hasítókulcs van, tehát 3 pointer, rendre az alábbi kulcsintervallumokkal:
    * (-∞, 2]
    * [3, 7]
    * [8, ∞)
* Érthető, hogy ily módon k db intervallumot k-1 db kulcs határoz meg
* Még két tulajdonság, ami talán nem is meglepő ezek után:
    * A kulcsok növekvő sorrendben vannak
    * Ráadásul ez szigorúan növekvő, azaz nincs ismétlődés (olyan van, hogy egy kulcs hasítókulcs is, és levélszinten is van; sőt olyan is van, hogy csak levélszinten van; és ami a legmeglepőbb, olyan is van, hogy csak hasítókulcs)

### Levélszint

* A levélszint kicsit másképp működik. Típusát tekintve ugyanolyan Node-okat tartalmaz ez is, de itt biztosak lehetünk benne, hogy ez nem lesz teljesen kihasználva
* A levélszinten ugyanis ugyanannyi pointer van, mint kulcs, de a kulcsok száma, ahogy az ábrákon is látszik, csak d-1 lehet. Tehát a pointerek számára is ez a maximum
* Egy köztes csúcs esetében a kulcsok hasítókulcsok, azaz a keresést irányító intervallumokat meghatározó segédértékek
* A levelek esetében már nincs tovább mit keresni, az i. kulcs az i. pointer által mutatott adat kulcsa
* Ebből az is következik, hogy a levélszint pointerei nem Node*-ok, hanem a konkrét, tárolt adatra mutató pointerek

```
 ___________
| 6 | 7 | 9 |
-------------
| *| *| *| /|
|__|__|__|__|
```

* Ebben a levélben tehát a maximális 3 db adatra van pointer, a 6-os, 7-es és 9-es kulcsúra
* Ebből az is következik egyébként, hogy 8-as kulcsú elem nincs a fában
* Van minimum is a pointerek/kulcsok számára a levélszinten (egy pointerű leveleket nem lenne gazdaságos létrehozni, fenntartani, főleg, hogy a minél alacsonyabb fára optimalizálunk)
* A levélszinten tehát ugyanannyi pointer és kulcs van, és nincsenek már intervallumok. A pointerek száma legalább alsóegészrész(d/2), legfeljebb d-1
* "Levélszintet" emlegettem, ezt azért tehettem meg, mert az ilyen fáknál minden levél azonos szinten van - a legalsón
    * Azaz minden keresés pontosan ugyanannyi lépéses, nagyságrendileg logaritmikus lesz
* A levélszinten található Node-okat szokás még összekötni egy pointerrel egy láncolt listát létrehozva, valahogy így:

```
 ___________    ___________    ___________
| 1 | 3 | / |  | 4 | 5 | 6 |  | 8 | 10| / |
-------------  -------------  -------------
| *| *| /| /|->| *| *| *| /|->| *| *| /| /|
|__|__|__|__|  |__|__|__|__|  |__|__|__|__|
```

* A tárolt elemeket így ezt a levélszintet alkotó láncolt listát végigjárva lehet kulcsaik szerint emelkedő sorrendben kiírni lineáris műveletigénnyel

### Gyökér

* Ha és amennyiben a gyökér egy levél is egyben (amíg legfeljebb d-1 elem van beszúrva), addig abban az értelemben levél, hogy annyi pointer lóg le róla, amennyi kulcsa van; de abban az értelemben nem, hogy lehet akár csak egy pointere is, nem muszáj elérni az alsóegészrész(d/2) kulcsot (mert nem is lehet az elején)

## Keresés

* Ha egy adott k kulcsot keresünk, kiindulunk a gyökérből
    * Ha a gyökér egyben levél is, megnézzük megvan-e benne a k, ha az i. kulcs a k, akkor az i. pointer mutat a keresett adatra
    * Ha nem levél, megnézzük, az első hasítóindexnél kisebb-e az elem. Ha kisebb, az első pointer felé megyünk és folytatjuk ugyanezt rekurzívan
    * Ha nem kisebb, és van második kulcs, azzal is összehasonlítjuk. Ha kisebb, vagy nincs 2. kulcs, a második pointer felé megyünk...
    * Ha van, és annál is nagyobb-egyenlő, akkor folytatjuk ugyanezt a 3. kulccsal
* A műveletigény logaritmusos egy igencsak jó konstans szorzóval

## Beszúrás

* Megkeressük a levélszinten a helyét
* Ha abban a Node-ban még volt szabad hely, csak beírjuk sorrendtartón
* Ha nem volt, ketté robbantjuk a Node-ot és az egyikbe írjuk be
* Felmegyünk a bejárt úton a gyökérhez, az új Node-ot regisztráljuk, és ha az azt szülő Node is betelt, azt is kettészedjük, stb.
* Bővebben lásd 07-es rész

## Törlés

* Kereséssel megkeressük a levélszinten a megfelelő helyet
* Ha megvan, kitöröljük és gyökértől vezető úton visszafelé összevonásokkal és egyéb trükkökkel helyreállítjuk az egyes csúcsok pointerszámaira való megkötéseket...
* Bővebben lásd 08-as rész