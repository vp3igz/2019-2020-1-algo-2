# Algoritmusok és adatszerkezetek 2.

## Harmadik téma - Általános fák és B+-fák - konzultáció

### Első feladat

* Írjunk algoritmust, ami kiszámolja a paraméterként megadott hagyományos (azaz "bináris láncolt") módon ábrázolt általános fa magasságát

#### Megoldás

* Előbb nézzünk egy iteratív megoldást, habár ebben is lesz rekurzív hívás
    * Ha a fa üres, akkor -1 a magasság
    * Ha nem, akkor mivel maga a fa a gyökerével van megadva, annak biztosan nincsen testvére, legfeljebb csak gyereke
    * Ebből az elgondolásból kiindulva azt fogjuk tenni, hogy egy p pointert rámutattatunk a t (első) gyerekére, ennek kiszámoljuk a magasságát (úgy értve, hogy ő egy fa gyökere, akinek a testvéreivel nem kell foglalkoznunk), majd továbblépünk ennek testvérére, annak is kiszámoljuk, stb.
    * A magasság kiszámolása egy rekurzív függvényhívás
    * A kiszámolt magasságokkal egy maximumkiválasztást végzünk
    * Végül az eredményhez hozzáadunk 1-et, az reprezentálja magát t-t

```
h(t : Node*) : Z
    HA t = NULL
        return -1
    KÜLÖNBEN
        height := -1
        p := t->child1
        AMÍG p != NULL
            h := h(p)
            HA h > height
                height := h
            p := p->sibling
        return h+1
```

* Egy sokkal elegánsabb megoldás a tiszta rekurzió
* Mivel ebben az ábrázolásban az általános fa felfogható egyfajta elforgatott bináris faként, használhatjuk az ott megtanult algoritmust...
* ... azzal a megkötéssel, hogy a siblingnél nem adunk hozzá egyet, hiszen az ebben a fában nem számít mélyebb szintnek

```
h(t : Node*) : Z
    HA t = NULL
        return -1
    KÜLÖNBEN
        return max(h(t->child1)+1, h(t->sibling))
```

```
max(a : Z, b : Z) : Z
    HA a > b
        return a
    KÜLÖNBEN
        return b
```

* A konzultáción felmerült még egy harmadik féle megoldás is: számoljuk meg a nem null "child1"-eket
    * Ezzel az a gond, hogy ha egy ágon eljutottunk mondjuk 3 child1-ig, de ennek a testvérében is van 3 child1, az csak 3 mélység, mégis 6-nak számoljuk
    * Ha annyiban módosítunk az elképzelésen, hogy csak "minden ágon" számolunk, akkor kb. meg is kaptuk az első algoritmust, ui. az a saját rekurziós szintjének height változóját -1-re inicializálja, és csak akkor nő vissza ez 0-ra legalább, ha a p nem null, a p pedig kezdetben a child1

### Második feladat

* Végezzük el az alábbi műveleteket a megadott kiinduló B+-fán (i - insert, d - delete)
    * <i-32, i-7, i-28, i-26, d-29, d-26, d-6, d-8, d-7>
    * Kiindulás:

```
        8/23/29
       /  / |  \
      /  /  |   \
     /  /   |    \
2/3/6 8/10 23/25 29/35/45
```

#### Megoldás

* i-32
    * 29 < 32, ezért a jobb szélsőbe szúrjuk be, ami viszont így megtelt, ketté kell választani
    * Lesz egy 29/32 és egy 35/45-ös csoport, ami miatt a szülőbe újonnan a 35 másolódik fel
    * A szülő is megtelt: 8/23/29/35
    * A szülő már nem levél, ezért a ketté választásánál lesz egy 8/23-as, és egy olyan, ahol a kisebbik kulcs nem másolódik, hanem felmegy az új gyökérbe, azaz egy 35-ös, és egy 29-es gyökér:

```
               29
              /   \
             /     \
         8/23       35
        / | \        | \
       /  |  \       |  \
      /   |   \      |   \
     /    |    \     |    \
2/3/6  8/10  23/25  29/32  35/45
```

* i-7
    * A legbaloldalibba kerül
    * Ami megint szétválik: 2/3 és 6/7-re
    * Az új hash-kulcs a szülőbe a 6-os
    * A szülőben ez el is fér, ezért itt megáll az algoritmus

```
              29
             /   \
            /     \
           /       \
       6/8/23       35
      / / | \        | \
     / /  |  \       |  \
    / /   |   \      |   \
   / /    |    \     |    \
2/3 6/7  8/10  23/25  29/32  35/45
```

* i-28
    * 28 < 29, ezért balra megyünk; 28 > 23, ezért jobbra
    * 23/25 mögé be is fér, kész vagyunk, nem frissül szülőben semmi

```
              29
             /   \
            /     \
           /       \
       6/8/23       35
      / / | \        \ \
     / /  |  \        \  \
    / /   |   \        \   \
   / /    |    \        \    \
2/3 6/7  8/10 23/25/28  29/32  35/45
```

* i-26
    * 26 < 29, ezért balra, 26 > 23, ezért jobbra megyünk
    * Betelt, kettéválunk: 23/25 és 26/28-ra
    * A szülőbe a 26 megy fel, tehát az is ketté válik: 6/8-ra és 23/26-ra válna, de ebből a kisebbik kulcs nem átmásolódik, hanem áthelyeződik a szülőjébe, tehát 26 marad csak itt, a szülőhöz kerül a 23, aminek van is ott helye

```
             23/29
            /  |   \
           /   |     \
          /    |       \
       6/8     26       35
     / | \     |  \      \ \
    /  |  \    |   \      \   \
   /   |   \   |    \      \    \
2/3 6/7  8/10 23/25 26/28  29/32  35/45
```

* *A konzultáción idáig hangzott el, de érdemes a későbbieket, ha más nem, vizsgára átnézni*
* d-29
    * 29 >= 29, ezért jobbra, majd 29 < 35, ezért balra megyünk
    * Kitöröljük a 29-et, 32 egyedül marad, és a szomszédjától (35/45) se tud lopni, egyesül hát a két levél
    * A szülőből törlődik a 35, mint hash-kulcs, tehát 0 kulcsa, 1 pointere van, ami nem kóser, átvenni nem tud, ezért egyesül a szomszéddal
    * A szomszéd kulcsa a 26, ez teljesen jó is, elválasztja az újdonsült 3 pointerű csúcs két bal oldali elemét. A jobb oldalit pedig a szülőből leeső 29-es fogja elválasztani

```
               23
              /  \
            /      \
          /          \
      6/8             26/29
     / | \           /  |  \
    /  |  \         /   |   \
   /   |   \       /    |    \
2/3 6/7  8/10   23/25  26/28  32/35/45
```

* d-26
    * 26 >= 23, ezért jobbra megyünk, majd mivel 26 >= 26 és 26 < 29, ezért a középső levelet választjuk
    * Kitöröljük a 26-ot, a 28 egyedül marad, de a jobb oldali szomszédjától át tudja venni a legkisebb elemet
    * Felfelé frissíteni kell a hash-kulcsot

```
               23
              /  \
            /      \
          /          \
      6/8             26/35
     / | \           /  |  \
    /  |  \         /   |   \
   /   |   \       /    |    \
2/3 6/7  8/10   23/25  28/32  35/45
```

* d-6
    * 6 < 23, ezért balra megyünk, 6 >= 6, de kiseb, mint 8, ezért utána középre
    * Töröljük a 6-ot, 7 egyedül marad, nem tudunk lopni, ezért egyesül a szomszédjával, megalkotva a 2/3/7-es levelet
    * Felfele ez abban nyilvánul meg, hogy a 6-os kulcs törlődik

```
           23
          /  \
         /     \
        /        \
       8          26/35
      / \        /   |  \
     /   \      /    |   \
    /     \    /     |    \
2/3/7  8/10   23/25 28/32  35/45
```

* d-8
    * 8 < 23, ezért balra, 8 >= 8, ezért jobbra megyünk, s töröljük a 8-at
    * 10 egyedül marad, átveszi a 7-est a szomszédtól
    * Felfelé frissül a határoló kulcs

```
           23
         /    \
        /      \
       /        \
      7         26/35
     /\        /   | \
    /  \      /    |  \
   /    \    /     |   \
2/3  7/10  23/25 28/32  35/45
```

* d-7
    * 7 < 23, ezért balra megyünk, saját magánál nem kisebb, ezért utána jobbra
    * Töröljük a 7-est, nem tud szomszédtól átvenni, ezért a 2/3/10 egyesül
    * Felfelé a 7-es köztes kulcs kiesik, mivel testvértől tud átvenni, ezt megteszi
    * Ilyenkor a hasító kulcsok úgy mozognak, hogy a szülő 23-asa megy a 7 helyére, a 26 pedig a szülőbe, ami helyébe nem kerül semmi, hiszen az ex-7-es megkapta a 23/25-öt, tehát itt csak a 26/35-ös csúcsnak csökken a gyerekszáma, s ezáltal a kulcsszáma

```
           26
         /    \
        /      \
       /        \
      23         35
     /\          /  \
    /  \        /    \
   /    \      /      \
2/3/10  23/25 28/32  35/45
```
