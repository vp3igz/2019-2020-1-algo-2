# Általános fa

## Alternatív reprezentáció

* Legyen most úgy megadva a fa, ahogy talán elsőre is logikus lenne:
    * Két adattagunk lesz, egy kulcs és egy SL (egyszerű láncolt lista) a gyerekekkel (ez felfogható rendezettnek)
    * A szülőt és a testvéreket nem tároljuk, illetve ha a szülőt tárolnánk, a testvéreket azzal könnyen el is érhetjük
* Úgymond formálisan:

```
Node
- key : T
- children : E1<Node*>*

+ Node() { children := NULL }
+ Node(k : T) { key := k, children := NULL }
```

## Példafeladat

* Állapítsuk meg, melyik node-nak van a legtöbb gyereke

```
getNodeWithMostChild(t : Node*) : Node*
	max := NULL
	count := -1
	getNodeWithMostChildPostorder(t, max, count)
	return max
```

```
getNodeWithMostChildPostorder(t:Node*, max& : Node*, c& : Z)
	HA t != NULL
		children := 0
		p := t->children
		AMÍG p != NULL
			children := children + 1
			getNodeWithMostChildPostorder(p->key, max, c)
			p := p->next
		HA children > c
			c := children
			max := t
```

* Ez egyben egy példa a postorder bejárásra ezzel a reprezentációval
* Az elején rögzítjük a max. Node-ot, mint NULL (akkor és csak akkor marad ez, ha üres a fa), valamint egy extremális értékre a maximum számot (amit hatékonysági okokból rakunk ki változóba)
* A postorder bejárás lokálisan számolja a gyerekeket, miközben meg is hívja a gyerekekre magát - arra figyeljünk, hogy itt minden rekurzív hívásnak saját "children" változója van, ami logikus is, hiszen a gyerekem gyereke (netán a testvérem gyereke) nem a gyerekem
* Kicsit így csalunk, hiszen a rekurzív hívás már tartalmazza a "process" egy részét - azaz a gyerekek számának meghatározását. A process további része, hogy lecseréljem a maxot és a c-t, ha a gyerekek száma nagyobb, mint az eddig számontartott maximum