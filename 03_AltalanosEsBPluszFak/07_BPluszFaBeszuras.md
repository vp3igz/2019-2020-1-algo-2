# B+-fa

* A fa foka legyen 4
* A fa csúcsait egyszerűen csak 1/2/3 alakban fogjuk jelölni, ahol a három szám a három kulcs
    * Ha egy kulcs hiányzik - mivel biztosan a végéről fog hiányozni -, nem írom ki

## Beszúrás

* Építsünk B+-fát az alábbi kulcsokból: <5, 10, 8, 7, 12, 11, 6, 4, 15, 16, 14, 13, 17, 18, 19, 20>

* 5
    * Ez levél és egyben gyökér lesz, ilyenkor a fának egy csúcsa van, annak egy indexe (ami nem hasító index) és egy pointere, az éles adatra

```
5
```

* 10
    * Mivel még nem telt be a csúcs, csak mellé írom

```
5/10
```

* 8
    * Mivel még mindig van hely, beírom, de az emelkedő sorrendet tartva

```
5/8/10
```

* 7
    * Ez sorrendet tartva 5/7/8/10 lenne, ami azt jelentené, hogy elértük a d db indexet, ami nem lehetséges
    * Ezért ketté válunk 5/7-re és 8/10-re
    * Mindkét csúcsban megvan a alsóegészrész(d/2) db elem, tehát jók vagyunk
    * Mivel kell egy gyökér, a két új csúcsot össze kell húzni egybe
    * A gyökérből két pointer megy ki, következésképp egy hasító index kell
    * Ez ilyenkor úgy áll elő, hogy a most összeálló két blokk közül a nagyobbnak a legkisebb indexét felmásoljuk a szülőbe
    * Esetünkben a nagyobb Node a 8/10-es, aminek a legkisebb indexe a 8, ez lesz tehát a gyökér indexe: ez jogos, mert a 8-nál nagyobb-egyenlő elemek lesznek a második részfában

```
    8
   / \
5/7   8/10
```

* 12
    * A 12 legalább 8, ezért jobbra megyünk, ott még van hely, ezért beszúrjuk sorrendtartón

```
    8
   / \
5/7   8/10/12
```

* 11
    * A 11 legalább 8, ezért jobbra megyünk, ott most kettéválunk 8/10 és 11/12 indexű Node-okra
    * Ahogy korábban is, a két új blokk közül a nagyobbik (11/12) legkisebb elemét (11) másoljuk fel a gyökérbe
    * Ami ezáltal 8/11 lett - még volt hely, ez így jó is

```
     8/11
   /  |   \
5/7  8/10  11/12
```

* 6
    * A 6 kisebb, mint 8, ezért balra megyünk, ott pedig van még hely

```
       8/11
     /  |   \
5/6/7  8/10  11/12
```

* 4
    * A 4 kisebb, mint 8, ezért balra megyünk
    * Egy újabb 4-est robbantunk szét: 4/5 és 6/7
    * A nagyobb blokk az utóbbi, a legkisebb indexe a 6-os, ez megy fel a gyökérbe

```
      6/ 8/ 11
     / |  |   \
    /  |  |    \
4/5  6/7  8/10  11/12
```

* 15
    * A 15 nagyobb minden indexnél, ezért legjobbra megyünk, oda el is fér

```
      6/ 8/ 11
     / |  |   \
    /  |  |    \
4/5  6/7  8/10  11/12/15
```

* 16
    * A 16 nagyobb minden indexnél, ezért legjobbra megyünk
    * Itt a 11/12 és a 15/16 válik szét
    * A szülőnek a nagyobb Node legkisebb indexét, a 15-öt másoljuk fel
    * A szülő ezzel az lenne, hogy 6/8/11/15, ami már nem fér el, ezért ez is feleződik
    * A 6/8-nak és a 11/15-nek közös szülő kell, aminek mivel két pointere van, ezért óhatatlanul egy kulcsa kell legyen, ami itt is a nagyobb gyerek legkisebb kulcsa
    * De! Ha az a gyerek, aminek a legkisebb kulcsa felmegy a szülőbe nem levél (most nem levél!), akkor nem másoljuk (hiszen a levelekben tároljuk effektíve a kulcsokat, a köztes csúcsokban csak útbaigazító szerepük van), hanem áthelyezzük ezt
    * Ezzel lesz egy egykulcsos és emiatt kétpointeres gyökerünk (azaz k=2, ami itt épp alsóegészrész(d/2), tehát még valid)
    * Valamint lesz annak két gyereke, egy kétkulcsos, 3 pointeres, és egy egykulcsos, két pointeres
    * A levélszinten pedig marad az a szabály, hogy ahány kulcs, annyi pointer

```
           11
          /  \
         /    \
        /      \
     6/8         15
    / | \        |  \
   /  |  \       |   \
4/5  6/7  8/10  11/12 15/16
```

* 14
    * 14 >= 11, ezért jobbra
    * 14 < 15, ezért balra
    * Ott van is hely

```
           11
          /   \
         /      \
        /         \
     6/8           15
    / | \        /    \
   /  |  \      /      \
4/5  6/7  8/10 11/12/14 15/16
```

* 13
    * 13 >= 11, ezért jobbra
    * 13 < 15, ezért balra
    * Kettéválunk 11/12 és 13/14-re
    * Levelekről lévén szó, felmásolódik a jobb Node legkisebb eleme (13)
    * Még van hely, ezért itt megállunk

```
              11
            /    \
          /        \
        /            \
     6/8              13/15
    / | \           /   |   \
   /  |  \         /    |    \
4/5  6/7  8/10  11/12  13/14  15/16
```

* 17
    * 17 >= 11, ezért jobbra
    * 17 >= 15, ezért legjobbra
    * Van pénz lóvéra

```
              11
            /    \
          /        \
        /            \
     6/8              13/15
    / | \           /   |   \
   /  |  \         /    |    \
4/5  6/7  8/10  11/12  13/14  15/16/17
```

* 18
    * 18 >= 11, ezért jobbra
    * 18 >= 15, ezért legjobbra
    * Kettéválunk: 15/16 és 17/18
    * Felmásolódik a 17, még van hely

```
              11
            /    \
          /        \
        /            \
     6/8               13/15/17
    / | \             / |  \    \
   /  |  \           /  |   \    \
4/5  6/7  8/10  11/12 13/14 15/16 17/18
```

* 19
    * 19 >= 11, ezért jobbra
    * 19 >= 17, ezért legjobbra
    * EZ

```
              11
            /    \
          /        \
        /            \
     6/8               13/15/17
    / | \             / |  \    \
   /  |  \           /  |   \    \
4/5  6/7  8/10  11/12 13/14 15/16 17/18/19
```

* 20
    * 20 >= 11, ezért jobbra
    * 20 >= 17, ezért legjobbra
    * Kettéválik a 17/18 és a 19/20 klán
    * Felmásolódik a 19
    * De itt is kettéválunk, megszületik a 13/15 és 17/19 Node
    * Felmozog (nem másolódik, mert nem levél!) a 17-es a 11-es mellé, ahol még van erre hely

```
                  11/17
                 /   |  \ 
               /     |    \
             /       |      \
           /         |        \
         /           |          \
     6/8             13/15        19
    / | \           /  |  \       |  \
   /  |  \         /   |   \      |    \
4/5  6/7  8/10  11/12 13/14 15/16 17/18 19/20
```

* Arra figyeljünk, hogy végig legyen érvényes az alábbi néhány invariáns (érdemes a 20-as beszúrása után keletkezett fán ezeket csekkolni):
    * Minden levél egy szinten van
    * Levélszinten nem lehet 1 kulcs (mert akkor 1 pointer is van, ami kisebb, mint alsóegészrész(d/2)) - kivéve ha gyökér is egyben
    * Nem levél szinten nem lehet 0 kulcs (mert akkor 1 pointer van, ami kisebb, mint alsóegészrész(d/2))
    * Balról jobbra összeolvasva az akár hasító, akár valódi kulcsokat, szigorúan monoton növő sorozatokat kapunk
    * Minden nemlevél csúcsnál amennyi kulcs van, eggyel több kimenő él (pointer) van
    * A legbaloldalibb részfában rekurzívan minden kulcs szigorúan kisebb, mint a legbaloldalibb szám
    * A legjobboldalibb részfában rekurzívan minden kulcs nagyobb-egyenlő, mint a legjobboldalibb szám
    * A középső részfá(k)ban a "részfa sorszáma"-1. hasítóindexnél nagyobb-egyenlő, a "részfa sorszáma". hasítóindexnél szigorúan kisebb kulcsok vannak